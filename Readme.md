Sceilig
===

![Sceilig](Houdini/Skellig_Michael03(js).jpg)

Sceilig is a procedural tool for recreating islands similar to [skellig michael](https://en.wikipedia.org/wiki/Skellig_Michael).

Because is it intended to be fully procedural, there are no `Heightfield Paint` or similar hardcoded data.

Requirements
----

|         Software            | Version  |
|-----------------------------|:--------:|
|         Houdini             | 18.5.408 |
|          Unreal             |  4.25.4  |
|Unreal Houdini Engine Plugin |    v2    |

Breakdown
----
* General Shape
  * 2 tetrahedrons
    * Smaller tetahedrons on the fringes
  * Stretched on the X axis
  * Smooth derivative
    * No sudden changes of slope
  * Strong ridge along the X axis
    * Do this by erosion via mask by feature angles
  * Valley between peaks
    * Should happen naturally cause of valley
* Details
  * Rocks except on lower slope places
  * Small rocks elsewhere
* Hives
  * Semi spherical

Problems & Solutions
----
* Problem
  * Erosion doesn't give the compressed cracked look we want
* Solution
  * Use mask by features to apply erosion only on certain angles
  * Add wirly noise at the end?
  * mask by feature to change response angle by slope
* Problem
  * No valley between mountains
* Solution
  * Apply mask by features by curvature to create valley
   * Use raindrop settings to create those "smooth" ramps

Drop algorithm
----
* Pick max distance, pick a random point and color points within distance.
* Repeat until no more points are close
* Get new color and pick uncolored point, until no points left
* Remove outliers (<10 points)

* If no suitable next point exists, quit